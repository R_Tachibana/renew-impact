// タイムスタンプ設定用
var toDoubleDigits = function (num) {
    num += "";
    if (num.length === 1) {
        num = "0" + num;
    }
    return num;
};
var TS = new Date();
$(function () {
    var TS = new Date();
    var yy = TS.getFullYear();
    $('#year').text('-' + yy);
});
var hoge = function (word) {
    var TS = new Date();
    var yy = TS.getFullYear();
    var mm = toDoubleDigits(TS.getMonth() + 1);
    var dd = toDoubleDigits(TS.getDate());
    var hh = toDoubleDigits(TS.getHours());
    var min = toDoubleDigits(TS.getMinutes());
    var sec = toDoubleDigits(TS.getSeconds());
    var mis = toDoubleDigits(TS.getMilliseconds()).slice(-2);
    var time = yy + '/' + mm + '/' + dd + ' ' + hh + ':' + min + ':' + sec + '.' + mis.trim(1);
    $('#time').text(time);
};
var timer = setInterval(hoge.bind(undefined, "hoge"), 3);
