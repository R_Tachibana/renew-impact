var _this = this;
var W = $(window).innerWidth();
var H = $(window).innerHeight();
var T = null;
var dy = $(this).scrollTop();
// let S1POS = $('#sec1').offset().top;
// let S2POS = $('#sec2').offset().top;
// let S3POS = $('#sec3').offset().top;
// let S4POS = $('#sec4').offset().top;
console.log('W: ' + W + ' / H: ' + H);
// onLoad
$(function () {
    $('#dy').text(dy);
    $('#vw').text(W);
    $('#vh').text(H);
    $('.nav').on('click', function () {
        if (W < 480) {
            $('#header .nav').toggleClass('is-active');
            console.log('clicked');
        }
        else {
            console.log('disable');
        }
    });
    $('.sections').removeClass('is-active');
    if (T !== null) {
        clearTimeout(T);
    }
    T = setTimeout(function () {
        $('.sections').addClass('is-active');
    }, 300);
});
// resize test
$(window).resize(function () {
    if (T !== null) {
        clearTimeout(T);
    }
    T = setTimeout(function () {
        W = $(window).innerWidth();
        H = $(window).innerHeight();
        $('#vw').text(W);
        $('#vh').text(H);
        console.log('resized');
        console.log('W: ' + W + ' / H: ' + H);
    }, 200);
    if (W < 480) {
        $('.nav').removeClass('is-active');
    }
    ;
});
$(window).scroll(function () {
    dy = $(_this).scrollTop();
    var S1POS = $('#sec1').offset().top;
    var S2POS = $('#sec2').offset().top;
    var S3POS = $('#sec3').offset().top;
    var S4POS = $('#sec4').offset().top;
    $('#dy').text(dy);
    $('#posS1').text(S1POS);
    $('#posS2').text(S2POS);
    $('#posS3').text(S3POS);
    $('#posS4').text(S4POS);
    // $('#sec1').text(sec1Top);
    if (dy >= 100) {
        $('#header').addClass('scrolled');
        var state = 'hidden';
    }
    else if (dy <= 50) {
        $('#header').removeClass('scrolled');
        var state = 'visible';
    }
    ;
    // if (dy >= S1POS) {
    //     $('#sec1').addClass('is-scrolled');
    // };
    // $('#header').css('top', 10 + dy / 2);
    if (T !== null) {
        clearTimeout(T);
    }
    T = setTimeout(function () {
        console.log('scrolled');
        console.log('dy :' + dy);
        console.log(state);
    }, 200);
});
