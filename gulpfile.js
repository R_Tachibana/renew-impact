// minimum config
var fs = require('fs');
var path = require('path');
var gulp = require('gulp');
var plumber = require('gulp-plumber');
var replace = require('gulp-replace');
var buffer = require('vinyl-buffer');
var sourcemaps = require('gulp-sourcemaps');
// minify
var stripComment = require('gulp-strip-comments');
// js
var tsc = require('gulp-typescript');
// css
var sass = require('gulp-sass');
var pcss = require('gulp-postcss');
// image
var sprite = require('gulp.spritesmith');
var imgmin = require('gulp-imagemin');
var imgminPng = require('imagemin-pngquant');
var imgminJpg = require('imagemin-mozjpeg');
// SourceFire
var IMG_DIR = './source/assets/sprite';
var IMG_FILE = IMG_DIR + '**/*.png';
var SCSS_DIR = './source/assets/scss/';
var SCSS_FILE = SCSS_DIR + '**/*.scss';
var CSS_DIR = './public/assets/css/';
var CSS_FILE = CSS_DIR + '*.css';
// Backup先指定
var BK_DIR = '../bk/';
// タイムスタンプ設定用
var toDoubleDigits = function (num) {
    num += "";
    if (num.length === 1) {
        num = "0" + num;
    }
    return num;
};
var TS = new Date();
var yy = TS.getFullYear();
var mm = toDoubleDigits(TS.getMonth() + 1);
var dd = toDoubleDigits(TS.getDate());
var hh = toDoubleDigits(TS.getHours());
var min = toDoubleDigits(TS.getMinutes());
var time = yy + mm + dd + '-' + hh + min;
//  Browser support
var browser = [
    "last 1 safari versions",
    "ie >= 11",
    "android >= 5"
];
// config path
var getFolders = function (dir_path) {
    return fs.readdirSync(dir_path).filter(function (file) {
        return fs.statSync(path.join(dir_path, file)).isDirectory();
    });
};
// 
gulp.task('compile-css', function () {
    gulp
        .src([SCSS_DIR] + 'main.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: 'expanded' }))
        .pipe(replace(/(\.\.\/){1,}img/g, '/img'))
        .pipe(pcss([
        require('css-mqpacker')({ short: true }),
        require('autoprefixer')({ browsers: browser })
    ]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('public/assets/css'))
        .pipe(gulp.dest('styleguide/components/css'));
});
// JS compile
gulp.task('compile-ts', function () {
    gulp.src('./source/assets/ts/*.ts')
        .pipe(tsc())
        .pipe(gulp.dest('./public/assets/js'));
});
// 'minify' -> prefix
gulp.task('cssMin', function () {
    gulp.src([CSS_FILE])
        .pipe(plumber())
        .pipe(pcss([
        require('css-mqpacker')({ short: true }),
        require('cssnano')(),
        require('autoprefixer')({ brousers: browser })
    ]))
        .pipe(gulp.dest(CSS_DIR));
});
// imgMin (24bit to 8bit)
gulp.task('imgMin', function () {
    gulp.src([IMG_FILE])
        .pipe(imgmin([
        imgminPng({
            quality: '50-70',
            speed: 1,
            floyd: 0
        }),
        imgminJpg({
            quality: '80-90',
            progressive: true
        }),
        imgmin.svgo(),
        imgmin.optipng()
    ]))
        .pipe(gulp.dest(IMG_DIR));
});
// htmlコメント削除、minify
gulp.task('htmlMin', function () {
    return gulp.src('public/page/**/*.html')
        .pipe(plumber())
        .pipe(stripComment())
        .pipe(replace(/[ \t]+\n/g, '\n'))
        .pipe(replace(/[ \t]{2,}/g, '  '))
        .pipe(replace(/\n+/g, '\n'))
        .pipe(gulp.dest('min/'));
});
// デバッグ環境適応時に実行
gulp.task('min', ['imgMin', 'htmlMin', 'cssMin']);
// 監視タスク
gulp.task('watch', function () {
    gulp.watch([SCSS_FILE], ['compile-css']),
        gulp.watch('source/assets/ts/*.ts', ['compile-ts']);
});
// default
gulp.task('default', ['watch']);
