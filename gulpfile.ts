// minimum config
const fs = require('fs')
const path = require('path');
const gulp = require('gulp');
const plumber = require('gulp-plumber');
const replace = require('gulp-replace');
const buffer = require('vinyl-buffer');
const sourcemaps = require('gulp-sourcemaps');

// minify
const stripComment = require('gulp-strip-comments');
// js
const tsc  = require('gulp-typescript');
// css
const sass = require('gulp-sass');
const pcss = require('gulp-postcss');

// image
const sprite = require('gulp.spritesmith');
const imgmin = require('gulp-imagemin');
const imgminPng = require('imagemin-pngquant');
const imgminJpg = require('imagemin-mozjpeg');

// SourceFire
const IMG_DIR = './source/assets/sprite';
const IMG_FILE = IMG_DIR + '**/*.png';
const SCSS_DIR = './source/assets/scss/';
const SCSS_FILE = SCSS_DIR + '**/*.scss';
const CSS_DIR = './public/assets/css/';
const CSS_FILE = CSS_DIR + '*.css';
// Backup先指定
const BK_DIR = '../bk/'

// タイムスタンプ設定用
var toDoubleDigits = function (num) {
    num += "";
    if (num.length === 1) {
        num = "0" + num;
    }
    return num;
};
const TS = new Date();
const yy = TS.getFullYear();
const mm = toDoubleDigits(TS.getMonth() + 1);
const dd = toDoubleDigits(TS.getDate());
const hh = toDoubleDigits(TS.getHours());
const min = toDoubleDigits(TS.getMinutes());
const time = yy + mm + dd + '-' + hh + min;

//  Browser support
var browser = [
    "last 1 safari versions",
    "ie >= 11",
    "android >= 5"
]

// config path
var getFolders = (dir_path) => {
    return fs.readdirSync(dir_path).filter((file) => {
        return fs.statSync(path.join(dir_path, file)).isDirectory();
    });
};

// 
gulp.task('compile-css', () => {
    gulp
        .src( [SCSS_DIR] + 'main.scss' )
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass( {outputStyle: 'expanded'} ))
        .pipe(replace(/(\.\.\/){1,}img/g, '/img'))
        .pipe(pcss([
            require('css-mqpacker')({ short: true }),
            require('autoprefixer')({ browsers: browser })
        ]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('public/assets/css'))
        .pipe(gulp.dest('styleguide/components/css'))
});
// JS compile
gulp.task('compile-ts', () => {
    gulp.src('./source/assets/ts/*.ts')
        .pipe(tsc())
        .pipe(gulp.dest('./public/assets/js'))
});

// 'minify' -> prefix
gulp.task('cssMin', () => {
    gulp.src([CSS_FILE])
        .pipe(plumber())
        .pipe(pcss([
            require('css-mqpacker')({ short: true }),
            require('cssnano')(),
            require('autoprefixer')({ brousers: browser })
        ]))
        .pipe(gulp.dest(CSS_DIR))
});

// imgMin (24bit to 8bit)
gulp.task('imgMin', () => {
    gulp.src([IMG_FILE])
        // .pipe(gulp.dest(BK_DIR + time))
        .pipe(imgmin([
            imgminPng({
                quality: '50-70',
                speed: 1,
                floyd: 0
            }),
            imgminJpg({
                quality: '80-90',
                progressive: true
            }),
            imgmin.svgo(),
            imgmin.optipng()
        ]))
        .pipe(gulp.dest(IMG_DIR));
});

// htmlコメント削除、minify
gulp.task('htmlMin', () => {
    return gulp.src('public/page/**/*.html')
        .pipe(plumber())
        // コメント,行末空白文字,空改行削除
        .pipe(stripComment())
        .pipe(replace(/[ \t]+\n/g, '\n'))
        .pipe(replace(/[ \t]{2,}/g, '  '))
        .pipe(replace(/\n+/g, '\n'))
        .pipe(gulp.dest('min/'))
});

// デバッグ環境適応時に実行
gulp.task('min', ['imgMin', 'htmlMin', 'cssMin']);

// 監視タスク
gulp.task('watch', () => {
    gulp.watch([SCSS_FILE], ['compile-css']),
    gulp.watch('source/assets/ts/*.ts', ['compile-ts'])
});
// default
gulp.task('default', ['watch']);