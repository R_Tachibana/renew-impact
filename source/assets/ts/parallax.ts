declare var jquery:any;

let W = $(window).innerWidth();
let H = $(window).innerHeight();
let T = null;
let dy = $(this).scrollTop();

// let S1POS = $('#sec1').offset().top;
// let S2POS = $('#sec2').offset().top;
// let S3POS = $('#sec3').offset().top;
// let S4POS = $('#sec4').offset().top;

console.log('W: ' + W + ' / H: ' + H);

// onLoad
$(() => {
    $('#dy').text(dy);
    $('#vw').text(W);
    $('#vh').text(H);
    $('.nav').on('click', () => {
        if (W < 480) {
            $('#header .nav').toggleClass('is-active');
            console.log('clicked');
        } else {
            console.log('disable');
        }
    });
    $('.sections').removeClass('is-active');
    if (T !== null) {
        clearTimeout(T);
    }
    T =setTimeout(()=>{
        $('.sections').addClass('is-active');
    }, 300);
});

// resize test
$(window).resize(()=> {
    if (T !== null) {
        clearTimeout(T);
    }
    T = setTimeout(()=> {
        W = $(window).innerWidth();
        H = $(window).innerHeight();
        $('#vw').text(W);
        $('#vh').text(H);
        console.log('resized');
        console.log('W: ' + W + ' / H: ' + H);
    }, 200);
    if (W < 480){
        $('.nav').removeClass('is-active');
    };
});

$(window).scroll(() => {
    dy = $(this).scrollTop();
    
    let S1POS = $('#sec1').offset().top;
    let S2POS = $('#sec2').offset().top;
    let S3POS = $('#sec3').offset().top;
    let S4POS = $('#sec4').offset().top;

    $('#dy').text(dy);
    $('#posS1').text(S1POS);
    $('#posS2').text(S2POS);
    $('#posS3').text(S3POS);
    $('#posS4').text(S4POS); 

    // $('#sec1').text(sec1Top);
    
    if (dy >= 100) {
        $('#header').addClass('scrolled');
        var state = 'hidden';
    } else if (dy <= 50) {
        $('#header').removeClass('scrolled');
        var state = 'visible';
    };
    // if (dy >= S1POS) {
    //     $('#sec1').addClass('is-scrolled');
    // };
    // $('#header').css('top', 10 + dy / 2);
    if (T !== null) {
        clearTimeout(T);
    }
    T = setTimeout(()=> {
        console.log('scrolled');
        console.log('dy :' + dy);
        console.log(state);
    }, 200);
});
