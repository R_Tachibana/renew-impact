declare var jquery: any;

// タイムスタンプ設定用
var toDoubleDigits = function (num) {
    num += "";
    if (num.length === 1) {
        num = "0" + num;
    }
    return num;
};

let TS = new Date();

$(() => {
    const TS = new Date();
    const yy = TS.getFullYear();
    $('#year').text('-' + yy);
});


var hoge = (word) => {
    const TS = new Date();
    const yy = TS.getFullYear();
    const mm = toDoubleDigits(TS.getMonth() + 1);
    const dd = toDoubleDigits(TS.getDate());
    const hh = toDoubleDigits(TS.getHours());
    const min = toDoubleDigits(TS.getMinutes());
    const sec = toDoubleDigits(TS.getSeconds());
    const mis = toDoubleDigits(TS.getMilliseconds()).slice(-2);
    const time = yy + '/' + mm + '/' + dd + ' ' + hh + ':' + min + ':' + sec + '.' + mis.trim(1);
    $('#time').text(time);
};
var timer = setInterval(hoge.bind(undefined, "hoge"), 3);